//Fetch throws back a promise, that's why we can use the then and catch
fetch('https://jsonplaceholder.typicode.com/posts/1')
// Use the "json" method from the "response" object to convert the data into json format to be used in our applications
.then(response => response.json())
//whatever is the result of previous then, we then take it and pass it to next then
.then(data => console.log(data))


//POST
//Fetch can have another arguement for POST, here you can see that the first arguement is the ENDPOINT, the second is the payload for POST
fetch('https://jsonplaceholder.typicode.com/posts',{
	method:'POST',
	headers:{
		'Content-type':'application/json'
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts object data into a stringfield JSON
	body: JSON.stringify({
		title:'New Post',
		body:'Hello World',
		userId:1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

//Updating a post using PUT method
// Updates a specific post following the Rest API (update, /posts/:id, PUT)

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PUT',
	headers:{
		"Content-type":'application/json'
	},
	body: JSON.stringify({
		id:1,
		title:'Updated Post',
		body:"Hello World",
		userId:1
	})
})
.then(response => response.json())
.then(data => console.log(data))

//Using patch (Specific updates/PUT is all of the document)
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:"PATCH",
	headers:{
		"Content-type":"application/json"
	},
	body: JSON.stringify({
		title:'Correct Title'
	})
})
.then(response => response.json())
.then(data => console.log(data))

//Delete
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'DELETE'
})
.then(response => response.json())
.then(data => console.log(data))

//Exercise: get 1 record
console.log('results')
fetch('https://jsonplaceholder.typicode.com/posts/50')
.then(response => response.json())
.then(data => console.log(data))
